import React, { Component } from 'react';
import './App.css';
import Pokemon from "./component/Pokemon";
import Pokemons from "./component/Pokemons";

class App extends Component {
  render() {
    return (
      <div className="App">
          <Pokemons/>
          <Pokemon />
      </div>
    );
  }
}

export default App;
