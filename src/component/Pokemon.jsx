import React, { Component } from 'react';
import GetPokemon from "../service/GetPokemon";
import axios from 'axios';

class Pokemon extends Component {

    constructor(props) {
        super(props);
        this.state = {
            allPokemons: [],
            data: [],
            abilities: [],
            sprites: [],
            error: ''
        };
    }

    componentDidMount() {
        axios.get('https://pokeapi.co/api/v2/pokemon/')
            .then((response) =>
            this.setState({allPokemons: response.data}));

        axios.get('https://pokeapi.co/api/v2/pokemon/pikachu')
            .then((response) =>
                this.setState({
                    data: response.data,
                    abilities: response.data.abilities,
                    sprites: response.data.sprites
                }))
            .catch(error => this.setState({error}))
    }

    render() {
        return (
            <div className={"container"}>
                <h3>{this.state.data.name}</h3>
                <div>{this.state.data.id}</div>
                <div>{this.state.data.base_experience}</div>
                <div>{this.state.abilities.map(ab => <li key={ab.url}>{ab.url}</li>)}</div>
                <div>
                    <img src={this.state.sprites.front_default} alt="pika"/>
                    <img src={this.state.sprites.back_default} alt="pika"/>
                </div>
                <div>
                    {/*{this.state.allPokemons.map(poke => <li key={poke.name}>{poke.name}</li>)}*/}
                </div>
            </div>
        )
    }
}

export default Pokemon
