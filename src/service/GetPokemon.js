import axios from 'axios';

class GetPokemon {

    getPokemon() {
        return axios.get('https://pokeapi.co/api/v2/pokemon/pikachu')
            .then(function (response) {
                console.log(response.data);
                return response.data;
                }
            );
    }
}

export default new GetPokemon()